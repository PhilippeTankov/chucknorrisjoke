// import React from 'react';
import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import axios from 'axios';

/* function App() {
  return (
    <div className="App">
      <h1 className="text-4xl font-bold testLol">Hello</h1>
    </div>
  );
} */

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fact: 'Chuck Norris a tout fait pour aider Philippe dans ce devoir, mais il n\'a pas réussi.'
    };
    this.getFact = this.getFact.bind(this);
  }

  getFact() {
    axios.get('https://api.chucknorris.io/jokes/random').then(response => {
      this.setState({fact: response.data.value});
    }).catch(error => {
      console.log(error);
    });
  }
  render() {
    return (
    /* Container */
    <div className="container p-3">
      {/* Title  */}
      <h1 className="has-text-centered has-text-black is-size-2">Chuck Norris Facts !</h1>
      <div class="box has-background-info has-text-white is-half has-text-centered">
      {/* button  */}
      <button type="button" className="button is-danger" onClick={this.getFact}>Click for more Facts!!</button>
        {/* content  */}
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #1</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #2</h2>
        <h1 className="has-text-centered is-size-3 pt-5 is-italic">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #3</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #4</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #5</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #6</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #7</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #8</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #9</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <h2 className="has-text-centered has-text-black is-size-2 is-italic">Fact #10</h2>
        <h1 className="has-text-centered is-size-3 pt-5">{'"' + this.state.fact + '"'}</h1>
        <br />
        {/* second button  */}
        <button type="button" className="button is-danger" onClick={this.getFact}>Click for more Facts!!</button>
      </div> 
    </div>
    );
  }

}


export default App;
